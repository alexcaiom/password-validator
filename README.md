## Sumário
* [password-validator](#password-validator)
* [Padrão](#padrão)
* [Problema](#problema)
* [Pontos cobertos](#pontos-cobertos)
* [Pontos a Ressaltar](#pontos-a-ressaltar)
* [Como executar o projeto](#Como-executar-o-projeto)

# password-validator
Projeto que usa o Spring Boot para validar senhas com o padrão abaixo.

# Padrão

Considere uma senha sendo válida quando a mesma possuir as seguintes definições:

- Nove ou mais caracteres
- Ao menos 1 dígito
- Ao menos 1 letra minúscula
- Ao menos 1 letra maiúscula
- Ao menos 1 caractere especial
  - Considere como especial os seguintes caracteres: !@#$%^&*()-+
- Não possuir caracteres repetidos dentro do conjunto

Exemplo:  

```c#
IsValid("") // false  
IsValid("aa") // false  
IsValid("ab") // false  
IsValid("AAAbbbCc") // false  
IsValid("AbTp9!foo") // false  
IsValid("AbTp9!foA") // false
IsValid("AbTp9 fok") // false
IsValid("AbTp9!fok") // true
```

> **_Nota:_**  Espaços em branco não devem ser considerados como caracteres válidos.

## Problema

A aplicação que expoe uma api web que valida se uma senha é válida.

Input: Uma senha (string).  
Output: Um boolean indicando se a senha é válida.

## Pontos cobertos

- Testes de unidade / integração
- Abstração, acoplamento, extensibilidade e coesão
- Design de API
- Clean Code
- SOLID
- Documentação da solução no *README* 

## Pontos a Ressaltar

Design Pattern: Chain of Responsibility 

Razão: Organização e componentização das condições, tornando-as configuráveis.

Abstração da Cadeia de Responsabilidades: como numa corrente, as validações representam os elos e só se passa para o próximo se a validação do atual passar e assim por diante.

Além disso, facilita a manutenção de regras (uma vez que basta incluir ou excluir uma classe e alterar a classe de controle (PasswordValidationChain).

Lógica de Validação: Regex (Expressões Regulares)

Razão: Padrão de mercado, fora não ter que se preocupar em fazer "N" if/elses e loops.



## Como executar o projeto
- Instalar JDK - Java Development Kit 11
- Instalar Maven
- Instalar STS - Spring Tool Suite
- Instalar Git
- Clonar este repositório
- Importar no STS
- Rodar no Boot Dashboard
- No cliente de sua preferência (Postman/ Soap UI), rodar:

POST:  http://localhost:8081/password-validator/password/validate

Payload:
```json 
{ 
  "password" : "{senha}" 
}
```
Observação: Alterar o texto {senha} pela a senha a ser testada.
