package br.com.massudaalexander.password.validation.validator;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import br.com.massudaalexander.password.validation.CommonValues;

class UpperCaseCharacterValidationTest {

	@InjectMocks UpperCaseCharacterValidation validation;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testValidate() {
		validation.validate(CommonValues.getPasswordValidateRequestDTO("password"), new ArrayList<String>());
	}

	@Test
	void testSetNext() {
		validation.setNext(validation);
	}

}
