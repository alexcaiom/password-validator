package br.com.massudaalexander.password.validation.validator;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.massudaalexander.password.validation.CommonValues;

class LowerCaseCharacterValidationTest {
	
	@InjectMocks LowerCaseCharacterValidation validation;
	@Mock SpecialCharacterValidation next;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testValidate() {
		testSetNext();
		List<String> messages = new ArrayList<String>();
		assertDoesNotThrow(() -> {
			validation.validate(CommonValues.getPasswordValidateRequestDTO("password"), messages);
		});
		assertTrue(messages.isEmpty(), "Should be empty");
		verify(next).validate(ArgumentMatchers.any(), ArgumentMatchers.anyList());;
	}

	@Test
	void testSetNext() {
		validation.setNext(next);
	}

}
