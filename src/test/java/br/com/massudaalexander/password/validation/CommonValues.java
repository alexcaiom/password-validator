/**
 * 
 */
package br.com.massudaalexander.password.validation;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;

/**
 * @author alex
 *
 */
public class CommonValues {

	public static PasswordValidateRequestDTO getPasswordValidateRequestDTO(String password) {
		PasswordValidateRequestDTO request = new PasswordValidateRequestDTO();
		request.setPassword(password);
		return request;
	}

}
