package br.com.massudaalexander.password.validation.validator.chain;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.massudaalexander.password.validation.CommonValues;
import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.validator.LowerCaseCharacterValidation;
import br.com.massudaalexander.password.validation.validator.NineCharactersValidation;
import br.com.massudaalexander.password.validation.validator.OneDigitValidation;
import br.com.massudaalexander.password.validation.validator.RepitedCharacterValidation;
import br.com.massudaalexander.password.validation.validator.SpecialCharacterValidation;
import br.com.massudaalexander.password.validation.validator.UpperCaseCharacterValidation;
import br.com.massudaalexander.password.validation.validator.WhitespaceCharactersValidation;

class PasswordValidationChainTest {
	
	@InjectMocks PasswordValidationChain chain;
	
	@Mock private NineCharactersValidation nineCharacters;
	@Mock private WhitespaceCharactersValidation whitespaceCharacters;
	@Mock private OneDigitValidation oneDigit;
	@Mock private UpperCaseCharacterValidation upperCaseCharacter;
	@Mock private LowerCaseCharacterValidation lowerCaseCharacter;
	@Mock private SpecialCharacterValidation specialCharacter;
	@Mock private RepitedCharacterValidation repitedCharacter;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testSetUp() {
		assertDoesNotThrow(() -> {
			chain.setUp();
		}, "Should not throw");
	}

	@Test
	void testValidate() {
		testSetUp();
		PasswordValidateRequestDTO request = CommonValues.getPasswordValidateRequestDTO("password");
		List<String> messages = new ArrayList<String>();
		chain.validate(request, messages);
	}

}
