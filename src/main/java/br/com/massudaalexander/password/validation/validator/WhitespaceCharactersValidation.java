/**
 * 
 */
package br.com.massudaalexander.password.validation.validator;

import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.validator.chain.PasswordValidation;

/**
 * @author Alex
 *
 */
@Component
public class WhitespaceCharactersValidation implements PasswordValidation {
	
	private static final String MESSAGE = "nenhum espaço em branco";
	private PasswordValidation next;

	@Override
	public void validate(PasswordValidateRequestDTO request, List<String> messages) {
		doValidate(request, messages);
		
		if (CollectionUtils.isEmpty(messages) && Objects.nonNull(next)) {
			next.validate(request, messages);
		}
	}

	private void doValidate(PasswordValidateRequestDTO request, List<String> messages) {
		if (StringUtils.isEmpty(request.getPassword()) 
				|| StringUtils.containsWhitespace(request.getPassword())) {
			messages.add("Digite uma senha com " + MESSAGE);
		}
	}

	@Override
	public void setNext(PasswordValidation validation) {
		this.next = validation;
	}

}
