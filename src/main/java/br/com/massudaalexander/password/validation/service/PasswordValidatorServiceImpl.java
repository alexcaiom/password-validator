package br.com.massudaalexander.password.validation.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.dto.PasswordValidateResponseDTO;
import br.com.massudaalexander.password.validation.validator.chain.PasswordValidationChain;

@Service
public class PasswordValidatorServiceImpl {
	
	@Autowired
	private PasswordValidationChain chain;
	
	public PasswordValidateResponseDTO validate(PasswordValidateRequestDTO request) {
		PasswordValidateResponseDTO response = new PasswordValidateResponseDTO(); 
		List<String> messages = new ArrayList<>();
		
		chain.validate(request, messages);
		
		response.setValid(CollectionUtils.isEmpty(messages));
		response.setMessages(messages);
		
		return response;
	}
	
	

}
