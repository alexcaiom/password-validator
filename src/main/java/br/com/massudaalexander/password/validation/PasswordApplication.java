package br.com.massudaalexander.password.validation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@SpringBootConfiguration
@EnableAutoConfiguration
@EnableWebMvc
@EnableAsync
@ComponentScan(basePackages = "br.com.massudaalexander.password.validation")
public class PasswordApplication {

    public static void main(String[] args) {
        SpringApplication.run(PasswordApplication.class, args);
    }
    
}
