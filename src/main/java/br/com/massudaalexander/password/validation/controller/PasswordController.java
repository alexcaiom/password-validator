package br.com.massudaalexander.password.validation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.dto.PasswordValidateResponseDTO;
import br.com.massudaalexander.password.validation.service.PasswordValidatorServiceImpl;

@RestController
@RequestMapping("/password")
public class PasswordController {
	
	@Autowired
	private PasswordValidatorServiceImpl service;
	
	@PostMapping("/validate")
	@ResponseBody
	public PasswordValidateResponseDTO validate(@RequestBody PasswordValidateRequestDTO request) {
		return service.validate(request);
	}

}
