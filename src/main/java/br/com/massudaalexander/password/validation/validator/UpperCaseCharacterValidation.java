/**
 * 
 */
package br.com.massudaalexander.password.validation.validator;

import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.validator.chain.PasswordValidation;

/**
 * @author Alex
 *
 */
@Component
public class UpperCaseCharacterValidation implements PasswordValidation {
	
	private static final String MESSAGE = "Ao menos 1 letra maiúscula";
	private PasswordValidation next;

	@Override
	public void validate(PasswordValidateRequestDTO request, List<String> messages) {
		doValidate(request, messages);
		
		if (CollectionUtils.isEmpty(messages) && Objects.nonNull(next)) {
			next.validate(request, messages);
		}
	}

	private void doValidate(PasswordValidateRequestDTO request, List<String> messages) {
		
		if (StringUtils.isEmpty(request.getPassword()) || !hasUpperCaseCharacter(request.getPassword())) {
			messages.add("Digite uma senha com " + MESSAGE);
		}
	}

	private boolean hasUpperCaseCharacter(String password) {
		String regex = "^(?=.*[A-Z])(?=.*\\d).+$";
		return Pattern.matches(regex, password);
	}

	@Override
	public void setNext(PasswordValidation validation) {
		this.next = validation;
	}

}
