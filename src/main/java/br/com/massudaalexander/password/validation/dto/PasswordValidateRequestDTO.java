/**
 * 
 */
package br.com.massudaalexander.password.validation.dto;

import lombok.Data;

/**
 * @author Alex
 *
 */
@Data
public class PasswordValidateRequestDTO {
	
	private String password;

}
