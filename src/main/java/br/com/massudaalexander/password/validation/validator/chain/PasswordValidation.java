package br.com.massudaalexander.password.validation.validator.chain;

import java.util.List;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;

public interface PasswordValidation {
	
	void validate(PasswordValidateRequestDTO request, List<String> messages);
	void setNext(PasswordValidation validation);

}
