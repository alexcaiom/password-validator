package br.com.massudaalexander.password.validation.validator.chain;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.validator.LowerCaseCharacterValidation;
import br.com.massudaalexander.password.validation.validator.NineCharactersValidation;
import br.com.massudaalexander.password.validation.validator.OneDigitValidation;
import br.com.massudaalexander.password.validation.validator.RepitedCharacterValidation;
import br.com.massudaalexander.password.validation.validator.SpecialCharacterValidation;
import br.com.massudaalexander.password.validation.validator.UpperCaseCharacterValidation;
import br.com.massudaalexander.password.validation.validator.WhitespaceCharactersValidation;

@Component
public class PasswordValidationChain {
	
	@Autowired
	private NineCharactersValidation nineCharacters;
	@Autowired
	private WhitespaceCharactersValidation whitespaceCharacters;
	@Autowired
	private OneDigitValidation oneDigit;
	@Autowired
	private UpperCaseCharacterValidation upperCaseCharacter;
	@Autowired
	private LowerCaseCharacterValidation lowerCaseCharacter;
	@Autowired
	private SpecialCharacterValidation specialCharacter;
	@Autowired
	private RepitedCharacterValidation repitedCharacter;
	
	@PostConstruct
	public void setUp() {
		nineCharacters.setNext(whitespaceCharacters);
		whitespaceCharacters.setNext(oneDigit);
		oneDigit.setNext(upperCaseCharacter);
		upperCaseCharacter.setNext(lowerCaseCharacter);
		lowerCaseCharacter.setNext(specialCharacter);
		specialCharacter.setNext(repitedCharacter);
	}

	public void validate(PasswordValidateRequestDTO request, List<String> messages) {
		nineCharacters.validate(request, messages);
	}

}
