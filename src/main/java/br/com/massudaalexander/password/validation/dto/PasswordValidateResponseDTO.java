/**
 * 
 */
package br.com.massudaalexander.password.validation.dto;

import java.util.List;

import lombok.Data;

/**
 * @author Alex
 *
 */
@Data
public class PasswordValidateResponseDTO {
	
	private boolean valid;
	private List<String> messages;

}
